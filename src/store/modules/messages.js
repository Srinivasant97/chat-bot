

const state = {
    message : [],
};

const getters = {
    getMessages(state){
        return state.message
    },
};

const actions = {
};

const mutations = {
    SET_MESSAGE(state,payload){
        state.message = [payload,...state.message]
    }
};

export default {
  state,
  getters,
  actions,
  mutations
};